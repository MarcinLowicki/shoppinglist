package com.mlsolution.shoppinglist.Entity;

import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table ( name = "shoppingtours")
public class ShoppingTour {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;
    private String name;
    private Date date;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shoppingtour", fetch = FetchType.EAGER)
    private Set<ShoppingTour> shoppingTours = new HashSet<>();

    @PersistenceConstructor
    public ShoppingTour(){

    }

}
