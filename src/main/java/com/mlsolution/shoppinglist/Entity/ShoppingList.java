package com.mlsolution.shoppinglist.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "shoppingLists")
public class ShoppingList {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;
    @NotNull
    private String name;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "shoppingtour_id")
    private ShoppingTour shoppingTour;

    @PersistenceConstructor
    public ShoppingList() {
    }
    public ShoppingList(@NotNull String name, ShoppingTour shoppingTour) {
        this.name = name;
        this.shoppingTour = shoppingTour;
    }
}
