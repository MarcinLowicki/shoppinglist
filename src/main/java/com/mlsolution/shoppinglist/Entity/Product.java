package com.mlsolution.shoppinglist.Entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import static javax.persistence.GenerationType.IDENTITY;

/*@Entity
@Table(name="products")*/
public class Product {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;
    private String productName;
    private long productAmount;
    private UnitOfMeasure unitOfMeasure;
    private boolean productIsInTheShoppingList;
    private boolean productIsInTheShoppingCart;

    public Product() {
        ;
    }
}
