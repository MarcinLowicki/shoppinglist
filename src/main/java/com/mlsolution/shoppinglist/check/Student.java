package com.mlsolution.shoppinglist.check;

import javax.persistence.*;
import javax.swing.*;
import java.util.HashSet;
import java.util.Set;


@Entity(name = "students")
public class Student {

    @Id
    private Long id;
    private String name;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST })
    @JoinTable(
            name = "users_groups",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "group_id")
    )
    private Set<Group> groups = new HashSet<>();

    public Student() {
    }
}
