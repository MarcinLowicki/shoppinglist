package com.mlsolution.shoppinglist.check;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "groups")
/*@Table*/
public class Group {

    @Id
    private Long id;
    private String name;

    @ManyToMany(mappedBy = "groups")
    private Set<Student> students = new HashSet<>();

    public Group() {
    }
}
